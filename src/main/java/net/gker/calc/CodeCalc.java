package net.gker.calc;

import net.liying.sourceCounter.SourceCounter;

import java.io.File;

import static net.liying.sourceCounter.SourceCounterKt.buildSourceCounter;

/**
 * 代码行数计算
 *
 * @Date 2020-01-16 14:25
 */
public class CodeCalc {

    /**
     * 对某一文件夹递归计算
     */
    public static void countDir(String dir) {
        countDir(new File(dir));
    }

    /**
     * 对某一文件夹递归计算
     */
    public static void countDir(File dir) {
        File[] files = dir.listFiles();
        for (File f : files) {
            if (f.isFile() && f.exists()) {
                countFile(f);
            } else if (f.isDirectory()) {
                countDir(f);
            }
        }
    }

    /**
     * 对某一文件夹计算当前目录
     */
    public static void countDirCurrent(String dir) {
        File[] files = new File(dir).listFiles();
        for (File f : files) {
            if (f.isFile() && f.exists()) {
                countFile(f);
            }
        }
    }

    /**
     * 对某一具体文件进行统计
     */
    public static void countFile(String file) {
        File f = new File(file);
        if (f.isFile() && f.exists()) {
            countFile(f);
        } else {
            System.out.println("countFile.文件不存在.file=" + file);
        }
    }

    /**
     * 对某一具体文件进行统计
     */
    private static void countFile(File file) {
        SourceCounter sourceCounter = buildSourceCounter(file, "UTF-8");
        sourceCounter.count();
        sourceCounter.getCountResult().print();
    }
}
